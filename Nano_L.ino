#include <Arduino_LSM9DS1.h>
#include <ArduinoBLE.h>

float x, y, z;

BLEService LSM9DS1("1812"); // "Human Interface Device" service

BLECharacteristic acceleration("2BF3", BLERead, 2); // "Health Sensor Feaures"

void setup() {
  
  // Serial.begin(9600);
  // while (!Serial);
  
  if (!IMU.begin() || !BLE.begin()) {
    // Serial.println("Failed to initialize!");
    while (1);
  }
  
  BLE.setLocalName("Nano-L");
  
  LSM9DS1.addCharacteristic(acceleration);
  BLE.addService(LSM9DS1);
  BLE.setAdvertisedService(LSM9DS1);
  BLE.setEventHandler(BLEConnected, startSampling);

  BLE.advertise();

  // Serial.println("Advertising...");
}

void loop() {
  // Polling for events
  BLE.poll();
}

void startSampling(BLEDevice forCentral) {

  int accX, accZ;

  while (BLE.connected()) {
      
    if (IMU.accelerationAvailable()) {
      IMU.readAcceleration(x, y, z);
      // Upscaling, due to float unreliability...
      // Y-axis values correspond to x-axis of interest, in this case
      accX = y * 100;
      accZ = z * 100;
    }

    // Updating characteristics
    byte data[2] = {(byte)accX,(byte)accZ}; // Float-to-byte conversion is still uncertain, in Arduino language... Better to convert from ints
    acceleration.writeValue(data,2);
    delay(10); // Sampling every 0.01s, because default frequency is 104 Hz
  }

}