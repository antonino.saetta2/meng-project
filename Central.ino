#include <ArduinoBLE.h>
#include <WiFiNINA.h>
#include "arduino_secrets.h"

// Shall be entered in Secret tab or arduino_secrets.h
char ssid[] = SECRET_SSID; // Network name
char pass[] = SECRET_PASS; // Network password (WPA)

int status = WL_IDLE_STATUS;

BLEDevice Nano_R, Nano_L;

BLECharacteristic rightAcc, leftAcc;

WiFiServer server(80);
WiFiClient laptop;

// For signal strength in communication
long rssi;
unsigned long ms;

// For Processing... Samples for 2-min sessions
byte rightDataX[1260], rightDataZ[1260];
byte leftDataX[1260], leftDataZ[1260];

int timer = 0; // In deciseconds

void setDelay(long RSSI) {

  if (RSSI > -32 ) {
    ms = 30;
  } else if (RSSI > -64) {
    ms = 45;
  } else if (RSSI > -96) {
    ms = 60;
  } else if (RSSI > -128) {
    ms = 75; // Maximum advisable, in this case
  } else {
    Serial.println("Communication is too weak");
    return;
  }

}

void startBLE() {
  // Initializing Bluetooth
  if (!BLE.begin()) {
    Serial.println("BLE initialization failed!");

    while (1);
  }

  BLE.setEventHandler(BLEDiscovered, arduinoExplorer);
  Serial.println("Scanning for Nano peripherals...");
  // Scanning without duplicates
  BLE.scanForUuid("1812"); // "Human Interface Device"
}

void board(BLEDevice nano_r, BLEDevice nano_l) {

  Serial.print("Connecting to Arduino boards... ");

  if (nano_r.connect() && nano_l.connect()) {
    Serial.println("Success!");
  } else {
  Serial.println("Failed to connect");
  return;
  }

  if (!nano_r.discoverAttributes() || !nano_l.discoverAttributes()) {
    
    Serial.println("Attribute discovery failed");
    nano_r.disconnect();
    nano_l.disconnect();
    return;
    
  } else {
    
    rightAcc = nano_r.characteristic("2BF3");
    leftAcc = nano_l.characteristic("2BF3");
    
  }

}

void switchToWiFi() {

  // Checking for the WiFi module
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    // Don't continue
    while (true);
  }

  if (WiFi.firmwareVersion() < WIFI_FIRMWARE_LATEST_VERSION) {
    Serial.println("Please upgrade the WiFi firmware!");
  }
  
  while (status != WL_CONNECTED) {
    
    Serial.print("Attempting to connect to network: ");
    Serial.println(ssid);
    
    // Connecting to WPA/WPA2 network:
    status = WiFi.begin(ssid, pass);

    // Waiting for 10s
    delay(10000);
  }

  Serial.print("Board's local IP: ");
  Serial.println(WiFi.localIP());
  server.begin();
  Serial.print("Listening on port 80... ");
}

void exploringAnkles(BLEDevice nano_r, BLEDevice nano_l) {
  
  while (timer < 1260) {

      rightAcc.read();
      leftAcc.read();
      rightDataX[timer] = rightAcc.value()[0]; // Corresponds to x(y)-axis value from sensor
      leftDataX[timer] = leftAcc.value()[0];
      rightDataZ[timer] = rightAcc.value()[1]; // Corresponds to z-axis value from sensor
      leftDataZ[timer] = leftAcc.value()[1];

      // Printing array values
      /*Serial.print("Left x-acceleration: ");
      Serial.println(leftDataX[timer], DEC);
      Serial.print("Left z-acceleration: ");
      Serial.println(leftDataZ[timer], DEC);
      Serial.print("Right x-acceleration: ");
      Serial.println(rightDataX[timer], DEC);
      Serial.print("Right z-acceleration: ");
      Serial.println(rightDataZ[timer], DEC);*/
          
      timer++; // Incrementing read count
      delay(60); // Sampling about every 0.1s...
  }

  Serial.print("Disconnecting from peripherals after ");
  Serial.print(timer);
  Serial.println(" cycles..."); // For debugging, in case any device disconnects
  nano_r.disconnect();
  nano_l.disconnect();
  BLE.end(); // Freeing the antenna
  delay(10000); // Giving time for the switch
      
  switchToWiFi();

  while (status == WL_CONNECTED) {

    laptop = server.available();
    
    if (laptop) {

      Serial.println("Found laptop");

      rssi = WiFi.RSSI();
      Serial.print("WiFi RSSI, in dBm: ");
      Serial.println(rssi);
      setDelay(rssi);

      // Handing value buffers over to the client
      Serial.print("Delivering right ankle values on x-axis...");

      for (int rx = 0; rx < 1260; rx++) {
        server.write(rightDataX[rx]);
        delay(ms);
      }
      Serial.println(" done");

      Serial.print("Delivering right ankle values on z-axis...");

      for (int rz = 0; rz < 1260; rz++) {
        server.write(rightDataZ[rz]);
        delay(ms);
      }
      Serial.println(" done");

      Serial.print("Delivering left ankle values on x-axis...");
      
      for (int lx = 0; lx < 1260; lx++) {
        server.write(leftDataX[lx]);
        delay(ms);
      }
      Serial.println(" done");

      Serial.print("Delivering left ankle values on z-axis...");
      
      for (int lz = 0; lz < 1260; lz++) {
        server.write(leftDataZ[lz]);
        delay(ms);
      }
      Serial.println(" done");

      break;
    }
  
  }

  WiFi.end();
  Serial.println("End of the program");
}

void setup() {
  Serial.begin(9600);
  while (!Serial);
  startBLE();
}

void loop() {
  // Polling for events
  BLE.poll();
}

// Using event handler instead of looping infinitely...
void arduinoExplorer(BLEDevice nano) {

  if (nano.localName() == "Nano-R") {
    Nano_R = nano;
  } else {
    Nano_L = nano;
  }

  if (Nano_R && Nano_L) {

    BLE.stopScan();

    // Confirming Nano details
    Serial.print("Found ");
    Serial.println(Nano_R.localName());
    Serial.print("Found ");
    Serial.println(Nano_L.localName());
    
    board(Nano_R,Nano_L);

    exploringAnkles(Nano_R,Nano_L);

  }

}