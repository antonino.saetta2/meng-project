import processing.net.*;
 // For the GUI
import java.util.*;
import controlP5.*;
//import g4p_controls.*;
import org.gicentre.utils.stat.*; // For the charts

Client laptop;

ControlP5 gui;

Textlabel plotLabel;

int[] dataIn; // Buffer storing the inputs from the server
int i = 0; // Counter for the buffer

// Arrays for the converted ankle data and sampling window
float[] dseconds, rightDataX, rightDataZ, leftDataX, leftDataZ;
float theta; // Setting the shoe-floor inclination (in radians)

XYChart lineChart;

boolean alreadyTransmitted = false, controller = false;

void setup() {
  
  size(1200, 480);
  noStroke(); // For the buttons
  gui = new ControlP5(this);
  
  dataIn = new int[5040]; // Cumulative size of all server data
  
  // Initializing arrays, size according to server
  rightDataX = new float[1260];
  rightDataZ = new float[1260];
  leftDataX = new float[1260];
  leftDataZ = new float[1260];
  dseconds = new float[1260]; // Sampling is in decisenconds, as timer in central samples every about 0.1s...
  
  // Filling GUI controllers
  plotLabel = gui.addTextlabel("plotLabel").setText("Plot a chart:").setPosition(280,0).setColorValue(0xff000000).setFont(createFont("Georgia",16));
  gui.addButton("right_x").setPosition(400,5).setSize(85,15).setValue(1);
  gui.addButton("left_x").setPosition(520,5).setSize(85,15).setValue(2);
  gui.addButton("right_z").setPosition(640,5).setSize(85,15).setValue(3);
  gui.addButton("left_z").setPosition(760,5).setSize(85,15).setValue(4);
  
  // Initializing plot
  textFont(createFont("Arial",10),10);  
  lineChart = new XYChart(this);
  lineChart.showXAxis(true); 
  lineChart.showYAxis(true);
  lineChart.setXFormat("00.0s");
  lineChart.setYFormat("###,## m/s^2");
  lineChart.setMinY(0);
  lineChart.setPointColour(color(180,50,50,100));
  lineChart.setPointSize(5);
  lineChart.setLineWidth(2);
  
  // Initializing client
  laptop = new Client(this, "192.168.178.40", 80); // Always check on server address
  laptop.write("Hi, please transmit"); // Summoning server
}

void draw() {
  
  background(255);
  
  if (!alreadyTransmitted) {

    while (laptop.active()) {

      if (laptop.available() > 0) {
      
        dataIn[i] = laptop.read();
        
        if (i < 1260) {
          dseconds[i] = (float)i/10.0; // Filling time samples array
        } else if (i == 5039) {
          println("Client finished");
          alreadyTransmitted = true;
          break;
        }
        
        i++;
      }

    }
    
    // Filling data arrays according to server order in buffer
    // e.g. right x-values first, then z-values etc...
    for (int rx = 0; rx < 1260; rx++) {
      rightDataX[rx] = abs(ms2(dataIn[rx]) * cos(theta)); // The modulus function ensures the values are not misinterpreted when e.g. turning back and front
    }
    
    for (int rz = 0; rz < 1260; rz++) {
      rightDataZ[rz] = (ms2(dataIn[rz+1260]) * sin(theta)) + 9.8; // Force of gravity contributes negatively to patient effort, so it has to be added back 
    }
    
    for (int lx = 0; lx < 1260; lx++) {
      leftDataX[lx] = abs(ms2(dataIn[lx+2520]) * cos(theta));
    }
    
    for (int lz = 0; lz < 1260; lz++) {
      leftDataZ[lz] = (ms2(dataIn[lz+3780]) * sin(theta)) + 9.8;
    }
    
  } else if (controller) { // If a plot has been chosen
      
     textSize(9);
     lineChart.draw(15,15,width-30,height-30);
     fill(120);

  }

}

// Converts int g data to float m/s^2
float ms2(int gs) {
  return (float)gs * 9.8/100.0;
}

// Computes average
float avg(float[] data) {
  float sum = 0.0;
  for (int i = 0; i < data.length; i++) {
    sum = sum + data[i];
  }
  return sum/(float)data.length;
}


// GUI controller implementations

public void right_x(int value) {
  
  if (alreadyTransmitted) {
    
    // Filling plot
    lineChart.setData(dseconds,rightDataX);
    
    println("Showing Right-Ankle acceleration over x-axis:");
    println("Average, highest peak and lowest peak values = " + avg(rightDataX) + " " + max(rightDataX) + " " + min(rightDataX));
    
    controller = true;
    redraw();
  }
  
}

public void left_x(int value) {
  
  if (alreadyTransmitted) {
    
    // Filling plot
    lineChart.setData(dseconds,leftDataX);
    
    println("Showing Left-Ankle acceleration over x-axis:");
    println("Average, highest peak and lowest peak values = " + avg(leftDataX) + " " + max(leftDataX) + " " + min(leftDataX));
    
    controller = true;
    redraw();
  }
  
}

public void right_z(int value) {
  
  if (alreadyTransmitted) {
    
    // Filling plot
    lineChart.setData(dseconds,rightDataZ);
    
    println("Showing Right-Ankle acceleration over z-axis:");
    println("Average, highest peak and lowest peak values = " + avg(rightDataZ) + " " + max(rightDataZ) + " " + min(rightDataZ));
    
    controller = true;
    redraw();
  }
  
}

public void left_z(int value) {
  
  if (alreadyTransmitted) {
    
    // Filling plot
    lineChart.setData(dseconds,leftDataZ);
    
    println("Showing Left-Ankle acceleration over z-axis:");
    println("Average, highest peak and lowest peak values = " + avg(leftDataZ) + " " + max(leftDataZ) + " " + min(leftDataZ));
    
    controller = true;
    redraw();
  }
  
}